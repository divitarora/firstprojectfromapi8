package com.we123.demoe123;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Firstprojectfromapi8Application {

	public static void main(String[] args) {
		SpringApplication.run(Firstprojectfromapi8Application.class, args);
	}

}
